# SKYRED Plantilla Dashboard

## Configuración de su entorno de desarrollo
Independientemente de la plataforma, debe tener acceso una copia desprotegida del código.
- Instalaremos xampp como servidor web local: https://www.apachefriends.org/es/download.html

### ... en Linux
  * Asegurate de tener los permisos necesarios sobre el directorio del proyecto, ejecuta `sudo chmod -R 777 dashboard-v1`
  * Clonar el repositorio en carpeta de instalacion de **xampp/htdocs** o **lampp/htdocs**

### ... en Windows
  * Instalar Git: https://gitforwindows.org/
  * Clonar el repositorio en carpeta de instalacion de **xampp/htdocs**

## Generar clave SSH
- https://gitlab.com/help/ssh/README#generating-a-new-ssh-key-pair

## Cómo clonar este repositorio¿?
  * `git clone --recurse-submodules git@gitlab.com:skyredweb/dashboard-v1.git`

Su entorno de desarrollo ya está configurado!

## Desarrollando
Edite su código en su escritorio usando cualquier editor de su elección.

Por favor, desarrolle nuevos modulos en nuevas ramas git y envíe solicitudes de fusion (merge request) contra la rama principal comentando cada actualización enviada.